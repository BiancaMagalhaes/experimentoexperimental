
pre = read.csv("https://gitlab.com/BiancaMagalhaes/experimentoux/-/raw/main/experimentoUX.csv")
pre2 = read.csv("https://gitlab.com/BiancaMagalhaes/experimentoux/-/raw/main/experimentoUXAplicativoOriginal.csv")
pre3 = read.csv("https://gitlab.com/BiancaMagalhaes/experimentoux/-/raw/main/experimentoUXAplicativoSimulado.csv")
pre4 = read.csv("https://gitlab.com/BiancaMagalhaes/experimentoux/-/raw/main/experimentoUXOriginal.csv")
pre5 = read.csv("https://gitlab.com/BiancaMagalhaes/experimentoux/-/raw/main/experimentoUXSimulado.csv")

y = pre[["Aplicativo"]]
x = pre[["Idade"]]

print(x)
print(y)

df <- data.frame(team=rep(c('A', 'B', 'C', 'D'), each=4),
                 pos=rep(c('G', 'F'), times=8),
                 points=round(runif(16, 4, 20),0))

head(df)

df <- data.frame(x, y)

cyl_freq <- c(4,1)

barplot(cyl_freq,
        horiz = FALSE,
        names.arg = c("Sim","Nao"),
        axis.lty = 1, offset = 0)
